package shay.com.turbinetest.adapters;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import shay.com.turbinetest.activities.InfoActivity;
import shay.com.turbinetest.objects.Ad;
import shay.com.turbinetest.R;
import shay.com.turbinetest.utils.Enums;

public class AdAdapter extends RecyclerView.Adapter<AdAdapter.AdViewHolder>
{
    private List<Ad> ads;

    public AdAdapter(List<Ad> ads)
    {
        this.ads = ads;
    }

    @NonNull
    @Override
    public AdViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        return new AdViewHolder(
                LayoutInflater.from(viewGroup.getContext())
                        .inflate(
                                R.layout.item_ad,
                                viewGroup,
                                false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdViewHolder adViewHolder, int i)
    {
        Ad ad = ads.get(i);
        float rating = Float.valueOf(ad.getRating());
        (adViewHolder).name.setText(ad.getProductName());
        (adViewHolder).rb.setRating(rating);
        Picasso.get()
                .load(ad.getProductThumbnail())
                .placeholder(R.drawable.picasso_loading_anim)
                .fit()
                .into((adViewHolder).image);
    }

    @Override
    public int getItemCount()
    {
        return ads.size();
    }

    class AdViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private TextView name;
        private ImageView image;
        private RatingBar rb;

        public AdViewHolder(@NonNull View itemView)
        {
            super(itemView);
            name    = itemView.findViewById(R.id.adNameTv);
            image   = itemView.findViewById(R.id.adThumbIv);
            rb      = itemView.findViewById(R.id.adRb);
            rb.setStepSize(0.5f);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            Intent infoIntent = new Intent(itemView.getContext(), InfoActivity.class);
            infoIntent.putExtra(Enums.KEY_PRODUCT_ID, Integer.valueOf(ads.get(getAdapterPosition()).getProductId()));
            itemView.getContext().startActivity(infoIntent);
        }
    }
}
