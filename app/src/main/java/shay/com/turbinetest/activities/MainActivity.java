package shay.com.turbinetest.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import shay.com.turbinetest.adapters.AdAdapter;
import shay.com.turbinetest.dataManager.Data;
import shay.com.turbinetest.network.ServerApi;
import shay.com.turbinetest.objects.Ad;
import shay.com.turbinetest.objects.AdsCallResponse;
import shay.com.turbinetest.R;
import shay.com.turbinetest.utils.Logger;

public class MainActivity extends AppCompatActivity
{
    private Data data;
    private RecyclerView adsRv;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadViews();
        loadData();
    }

    private void loadData()
    {
        data = Data.getInstance();
        getAds();
    }

    private void loadViews()
    {
        adsRv = findViewById(R.id.adsRv);
    }

    public void getAds()
    {
        try
        {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://ads.appia.com/")
                    .client(new OkHttpClient())
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .build();

            ServerApi api = retrofit.create(ServerApi.class);

            Call<AdsCallResponse> call = api.getAdds();
            call.enqueue(new Callback<AdsCallResponse>()
            {
                @Override
                public void onResponse(Call<AdsCallResponse> call, retrofit2.Response<AdsCallResponse> response)
                {
                    if (response.body() != null)
                    {
                        if (response.body().getAds() != null)
                        {
                            if (response.body().getAds().size() > 0)
                            {
                                if (data.getAds() == null)
                                {
                                    data.setAds(response.body().getAds());
                                }
                                else
                                {
                                    List<Ad> updatedAds = response.body().getAds();
                                    List<Ad> adsToAdd = new ArrayList<>();
                                    for (int i = 0; i < updatedAds.size(); i++)
                                    {
                                        boolean exists = false;
                                        for (int j = 0; j < data.getAds().size(); j++)
                                        {
                                            Ad newAd = updatedAds.get(i);
                                            Ad oldAd = data.getAds().get(j);
                                            if (newAd.productId.equals(oldAd.productId))
                                            {
                                                exists = true;
                                            }
                                        }
                                        if (!exists)
                                        {
                                            adsToAdd.add(updatedAds.get(i));
                                        }
                                    }
                                    if (adsToAdd.size() > 1)
                                    {
                                        data.getAds().addAll(adsToAdd);
                                        if (adsRv != null)
                                        {
                                            adsRv.getAdapter().notifyDataSetChanged();
                                        }
                                    }
                                }
                                adsRv.setAdapter(new AdAdapter(data.getAds()));
                                adsRv.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                            }
                        }
                    }
                    else
                    {
                        Toast.makeText(MainActivity.this, "No ads to show", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AdsCallResponse> call, Throwable t)
                {
                    Logger.Print("OnError: " + t.getMessage());
                }
            });

        }
        catch (Exception e)
        {
            Logger.printStackTrace(e);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (data != null)
        {
            if (data.getAds() != null)
            {
//                Check if there are new ads, and refresh the recycler view.
                getAds();
            }
        }
    }
}
