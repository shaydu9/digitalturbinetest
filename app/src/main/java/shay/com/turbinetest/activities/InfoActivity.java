package shay.com.turbinetest.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import shay.com.turbinetest.R;
import shay.com.turbinetest.dataManager.Data;
import shay.com.turbinetest.databinding.ActivityInfoBinding;
import shay.com.turbinetest.objects.Ad;
import shay.com.turbinetest.utils.Enums;

public class InfoActivity extends AppCompatActivity
{
    private int productId;
    private Data data;
    private ActivityInfoBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_info);
        loadData();
    }

    private void loadData()
    {
        data = Data.getInstance();
        productId = getIntent().getIntExtra(Enums.KEY_PRODUCT_ID, -1);
        if (productId != -1)
        {
            for (Ad ad : data.getAds())
            {
                if (productId == Integer.valueOf(ad.getProductId()))
                {
                    binding.setAd(ad);
                    break;
                }
            }
        }
    }
}
