package shay.com.turbinetest.dataManager;

import java.util.List;

import shay.com.turbinetest.objects.Ad;

public class Data
{
    private static Data instance = new Data();

    public static Data getInstance()
    {
        if (instance == null)
        {
            instance = new Data();
        }

        return instance;
    }

    private List<Ad> ads;

    public List<Ad> getAds()
    {
        return ads;
    }

    public void setAds(List<Ad> ads)
    {
        this.ads = ads;
    }
}
