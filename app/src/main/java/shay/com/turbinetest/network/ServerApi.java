package shay.com.turbinetest.network;

import retrofit2.Call;
import retrofit2.http.GET;
import shay.com.turbinetest.objects.AdsCallResponse;
import shay.com.turbinetest.utils.Enums;

public interface ServerApi
{
    @GET (Enums.HTTP_PARAMS)
    Call<AdsCallResponse> getAdds();
}
