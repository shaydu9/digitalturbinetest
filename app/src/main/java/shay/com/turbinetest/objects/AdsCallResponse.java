package shay.com.turbinetest.objects;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root (name = "ads")
public class AdsCallResponse
{
    @Element(name = "version", required = false)
    private String version;
    @Element(name = "xmlns", required = false)
    private String xmlns;
    @Element(name = "serverId", required = false)
    private String serverId;
    @Element(name = "responseTime", required = false)
    private String responseTime;
    @Element(name = "totalCampaignsRequested", required = false)
    private String totalCampaignsRequested;
    @ElementList(name = "ads", inline = true, required = false)
    private List<Ad> ads;

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getXmlns()
    {
        return xmlns;
    }

    public void setXmlns(String xmlns)
    {
        this.xmlns = xmlns;
    }

    public String getServerId()
    {
        return serverId;
    }

    public void setServerId(String serverId)
    {
        this.serverId = serverId;
    }

    public String getResponseTime()
    {
        return responseTime;
    }

    public void setResponseTime(String responseTime)
    {
        this.responseTime = responseTime;
    }

    public String getTotalCampaignsRequested()
    {
        return totalCampaignsRequested;
    }

    public void setTotalCampaignsRequested(String totalCampaignsRequested)
    {
        this.totalCampaignsRequested = totalCampaignsRequested;
    }

    public List<Ad> getAds()
    {
        return ads;
    }

    public void setAds(List<Ad> ads)
    {
        this.ads = ads;
    }
}
