package shay.com.turbinetest.objects;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import shay.com.turbinetest.utils.Logger;

@Root (name = "ad")
public class Ad
{
    @Element (name = "campaignId", required = false)
    public String campaignId;
    @Element (name = "appId", required = false)
    public String appId;
    @Element (name = "campaignTypeId", required = false)
    public String campaignTypeId;
    @Element (name = "productDescription", required = false)
    public String productDescription;
    @Element (name = "productId", required = false)
    public String productId;
    @Element (name = "isRandomPick", required = false)
    public String isRandomPick;
    @Element (name = "callToAction", required = false)
    public String callToAction;
    @Element (name = "categoryName", required = false)
    public String categoryName;
    @Element (name = "bidRate", required = false)
    public String bidRate;
    @Element (name = "minOSVersion", required = false)
    public String minOSVersion;
    @Element (name = "creativeId", required = false)
    public String creativeId;
    @Element (name = "campaignDisplayOrder", required = false)
    public String campaignDisplayOrder;
    @Element (name = "averageRatingImageURL", required = false)
    public String averageRatingImageURL;
    @Element (name = "clickProxyURL", required = false)
    public String clickProxyURL;
    @Element (name = "rating", required = false)
    public String rating;
    @Element (name = "productName", required = false)
    public String productName;
    @Element (name = "appPrivacyPolicyUrl", required = false)
    public String appPrivacyPolicyUrl;
    @Element (name = "homeScreen", required = false)
    public String homeScreen;
    @Element (name = "billingTypeId", required = false)
    public String billingTypeId;
    @Element (name = "impressionTrackingURL", required = false)
    public String impressionTrackingURL;
    @Element (name = "numberOfRatings", required = false)
    public String numberOfRatings;
    @Element (name = "productThumbnail", required = false)
    public String productThumbnail;

    public Ad()
    {
    }

    public Ad(String campaignId, String appId, String campaignTypeId, String productDescription, String productId, String isRandomPick, String callToAction, String categoryName, String bidRate, String creativeId, String campaignDisplayOrder, String averageRatingImageURL, String clickProxyURL, String rating, String productName, String homeScreen, String billingTypeId, String impressionTrackingURL, String numberOfRatings, String productThumbnail)
    {
        this.campaignId = campaignId;
        this.appId = appId;
        this.campaignTypeId = campaignTypeId;
        this.productDescription = productDescription;
        this.productId = productId;
        this.isRandomPick = isRandomPick;
        this.callToAction = callToAction;
        this.categoryName = categoryName;
        this.bidRate = bidRate;
        this.creativeId = creativeId;
        this.campaignDisplayOrder = campaignDisplayOrder;
        this.averageRatingImageURL = averageRatingImageURL;
        this.clickProxyURL = clickProxyURL;
        this.rating = rating;
        this.productName = productName;
        this.homeScreen = homeScreen;
        this.billingTypeId = billingTypeId;
        this.impressionTrackingURL = impressionTrackingURL;
        this.numberOfRatings = numberOfRatings;
        this.productThumbnail = productThumbnail;
    }

    public String getCampaignId ()
    {
        return campaignId;
    }

    public void setCampaignId (String campaignId)
    {
        this.campaignId = campaignId;
    }

    public String getAppId ()
    {
        return appId;
    }

    public void setAppId (String appId)
    {
        this.appId = appId;
    }

    public String getCampaignTypeId ()
    {
        return campaignTypeId;
    }

    public void setCampaignTypeId (String campaignTypeId)
    {
        this.campaignTypeId = campaignTypeId;
    }

    public String getProductDescription ()
    {
        return productDescription;
    }

    public void setProductDescription (String productDescription)
    {
        this.productDescription = productDescription;
    }

    public String getProductId ()
    {
        return productId;
    }

    public void setProductId (String productId)
    {
        this.productId = productId;
    }

    public String getIsRandomPick ()
    {
        return isRandomPick;
    }

    public void setIsRandomPick (String isRandomPick)
    {
        this.isRandomPick = isRandomPick;
    }

    public String getCallToAction ()
    {
        return callToAction;
    }

    public void setCallToAction (String callToAction)
    {
        this.callToAction = callToAction;
    }

    public String getCategoryName ()
    {
        return categoryName;
    }

    public void setCategoryName (String categoryName)
    {
        this.categoryName = categoryName;
    }

    public String getBidRate ()
    {
        return bidRate;
    }

    public void setBidRate (String bidRate)
    {
        this.bidRate = bidRate;
    }

    public String getCreativeId ()
    {
        return creativeId;
    }

    public void setCreativeId (String creativeId)
    {
        this.creativeId = creativeId;
    }

    public String getCampaignDisplayOrder ()
    {
        return campaignDisplayOrder;
    }

    public void setCampaignDisplayOrder (String campaignDisplayOrder)
    {
        this.campaignDisplayOrder = campaignDisplayOrder;
    }

    public String getAverageRatingImageURL ()
    {
        return averageRatingImageURL;
    }

    public void setAverageRatingImageURL (String averageRatingImageURL)
    {
        this.averageRatingImageURL = averageRatingImageURL;
    }

    public String getClickProxyURL ()
    {
        return clickProxyURL;
    }

    public void setClickProxyURL (String clickProxyURL)
    {
        this.clickProxyURL = clickProxyURL;
    }

    public String getRating ()
    {
        return rating;
    }

    public void setRating (String rating)
    {
        this.rating = rating;
    }

    public String getProductName ()
    {
        return productName;
    }

    public void setProductName (String productName)
    {
        this.productName = productName;
    }

    public String getHomeScreen ()
    {
        return homeScreen;
    }

    public void setHomeScreen (String homeScreen)
    {
        this.homeScreen = homeScreen;
    }

    public String getBillingTypeId ()
    {
        return billingTypeId;
    }

    public void setBillingTypeId (String billingTypeId)
    {
        this.billingTypeId = billingTypeId;
    }

    public String getImpressionTrackingURL ()
    {
        return impressionTrackingURL;
    }

    public void setImpressionTrackingURL (String impressionTrackingURL)
    {
        this.impressionTrackingURL = impressionTrackingURL;
    }

    public String getNumberOfRatings ()
    {
        return numberOfRatings;
    }

    public void setNumberOfRatings (String numberOfRatings)
    {
        this.numberOfRatings = numberOfRatings;
    }

    public String getProductThumbnail ()
    {
        return productThumbnail;
    }

    public void setProductThumbnail (String productThumbnail)
    {
        this.productThumbnail = productThumbnail;
    }
}
