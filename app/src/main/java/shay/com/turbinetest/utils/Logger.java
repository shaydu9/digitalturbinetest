package shay.com.turbinetest.utils;

import android.util.Log;

public class Logger
{
    private static final boolean LOGGER_ON = true;

    public static void Print(String text)
    {
        if (LOGGER_ON)
            Log.d("Turbine: ", text);
    }

    public static void printStackTrace(Exception e)
    {
        if (LOGGER_ON)
        {
            e.printStackTrace();
        }
    }
}
